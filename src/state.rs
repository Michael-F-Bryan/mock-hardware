//! Information used to record the state of the world.

use arrayvec::ArrayVec;
use core::ops::Index;

/// The world state.
#[derive(Debug, Default, Clone, PartialEq)]
pub struct State {
    pub axes: ArrayVec<[Axis; 8]>,
    pub inputs: BitVector<[u8; 4]>,
    pub outputs: BitVector<[u8; 4]>,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Axis {
    pub position: f32,
}

#[derive(Debug, Default, Copy, Clone, PartialEq, Eq)]
pub struct BitVector<A>(A);

impl<A: Default> BitVector<A> {
    pub fn zeroed() -> BitVector<A> {
        BitVector(A::default())
    }
}

impl<A: AsRef<[u8]>> BitVector<A> {
    pub fn len(&self) -> usize {
        self.0.as_ref().len() * 8
    }

    pub fn get(&self, ix: usize) -> Option<bool> {
        let byte = ix / 8;
        let bit = ix % 8;

        self.0.as_ref().get(byte).map(|b| b & (1 << bit) != 0)
    }

    pub fn iter<'this>(
        &'this self,
    ) -> impl Iterator<Item = (usize, bool)> + 'this {
        (0..self.len()).map(move |ix| (ix, self[ix]))
    }
}

impl<A: AsRef<[u8]> + AsMut<[u8]>> BitVector<A> {
    pub fn set(&mut self, ix: usize, state: bool) {
        let byte = ix / 8;
        let bit = ix % 8;

        if let Some(b) = self.0.as_mut().get_mut(byte) {
            let mask = 1 << bit;
            if state {
                *b |= mask;
            } else {
                *b &= !mask;
            }
        }
    }
}

impl<A: AsRef<[u8]>> Index<usize> for BitVector<A> {
    type Output = bool;

    fn index(&self, ix: usize) -> &Self::Output {
        if self.get(ix).unwrap() {
            &true
        } else {
            &false
        }
    }
}
