use super::DigitalOutput;
use crate::{Continuation, Task};

/// Set a single output and stop the task.
pub fn set_output<A, S>(output_no: usize, state: bool) -> impl Task<A, S>
where
    A: DigitalOutput + ?Sized,
    S: ?Sized,
{
    SetOutput { output_no, state }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct SetOutput {
    output_no: usize,
    state: bool,
}

impl<A, S> Task<A, S> for SetOutput
where
    A: DigitalOutput + ?Sized,
    S: ?Sized,
{
    fn tick(&mut self, _: &S, triggers: &mut A) -> Continuation {
        triggers.set_output(self.output_no, self.state);
        Continuation::Exit
    }
}
