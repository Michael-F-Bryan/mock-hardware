use super::AxisMotion;
use crate::state::{Axis, State};
use crate::{Continuation, Task};

/// A `Task` which will trigger a limit switch whenever it goes outside a
/// pre-defined range.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct AxisLimits {
    pub axis_no: usize,
    pub lower_bound: f32,
    pub upper_bound: f32,
}

impl<A> Task<A, Axis> for AxisLimits
where
    A: AxisMotion + ?Sized,
{
    fn tick(&mut self, ax: &Axis, triggers: &mut A) -> Continuation {
        let pos = ax.position;
        let out_of_bounds = pos < self.lower_bound
            || self.upper_bound < pos
            || !pos.is_normal();

        triggers.set_limit_switch(self.axis_no, out_of_bounds);

        Continuation::Continue
    }
}

impl<A> Task<A> for AxisLimits
where
    A: AxisMotion + ?Sized,
{
    fn tick(&mut self, state: &State, triggers: &mut A) -> Continuation {
        if let Some(ax) = state.axes.get(self.axis_no) {
            self.tick(ax, triggers);
        }

        Continuation::Continue
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::prelude::v1::*;

    #[derive(Debug, Default)]
    struct SpyAxisMotion {
        limit_switches: Vec<(usize, bool)>,
    }

    impl AxisMotion for SpyAxisMotion {
        fn set_limit_switch(&mut self, axis_no: usize, is_on_limits: bool) {
            self.limit_switches.push((axis_no, is_on_limits));
        }
    }

    #[test]
    fn do_nothing_when_inside_bounds() {
        let mut spy = SpyAxisMotion::default();
        let axis = Axis { position: 0.5 };
        let mut axis_limits = AxisLimits {
            axis_no: 0,
            lower_bound: 0.0,
            upper_bound: 1.0,
        };

        let cont = axis_limits.tick(&axis, &mut spy);

        assert_eq!(cont, Continuation::Continue);
        assert_eq!(spy.limit_switches.len(), 1);
        let (axis_no, state) = spy.limit_switches[0];
        assert_eq!(axis_no, axis_limits.axis_no);
        assert!(!state);
    }

    #[test]
    fn halt_axis_when_out_of_bounds() {
        let inputs = vec![-50.0, 100.0, std::f32::INFINITY, std::f32::NAN];

        for position in inputs {
            let mut spy = SpyAxisMotion::default();
            let axis = Axis { position };
            let mut axis_limits = AxisLimits {
                axis_no: 0,
                lower_bound: 0.0,
                upper_bound: 1.0,
            };

            let cont = axis_limits.tick(&axis, &mut spy);

            assert_eq!(cont, Continuation::Continue);
            let (axis_no, state) = spy.limit_switches[0];
            assert_eq!(axis_no, axis_limits.axis_no);
            assert!(state);
        }
    }
}
