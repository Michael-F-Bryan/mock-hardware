use crate::{Continuation, Task};

/// Create a task which wil wait until its predicate is satisfied before
/// stopping.
pub fn wait_until<P, A, S>(predicate: P) -> impl Task<A, S>
where
    P: FnMut(&S) -> bool,
{
    WaitUntil(predicate)
}

struct WaitUntil<P>(P);

impl<P, A, S> Task<A, S> for WaitUntil<P>
where
    P: FnMut(&S) -> bool,
{
    fn tick(&mut self, state: &S, _: &mut A) -> Continuation {
        if self.0(state) {
            Continuation::Exit
        } else {
            Continuation::Continue
        }
    }
}
