//! Composable tasks for building up task sequences.
//!
//! # Examples
//!
//! Tasks can be combined using smaller tasks and the various `TaskExt` methods
//! to create larger automation sequences.
//!
//! ```rust
//! use mock_hardware::{Task, TaskExt, Continuation};
//! use mock_hardware::tasks::{self, DigitalOutput};
//! use mock_hardware::state::{State, BitVector};
//!
//! /// Dummy digital output trigger.
//! #[derive(Debug, Default)]
//! struct Outputs(BitVector<[u8; 4]>);
//!
//! impl DigitalOutput for Outputs {
//!   fn set_output(&mut self, output: usize, state: bool) { self.0.set(output, state); }
//! }
//!
//! // set up our world and triggers
//! let mut state = State::default();
//! let mut outputs = Outputs::default();
//!
//! // wait until input 4 is LOW and then set output 5 HIGH.
//! let mut t = tasks::wait_until(|state: &State| state.inputs[4] == false)
//!     .and_then(tasks::set_output(5, true));
//!
//! state.inputs.set(4, true);
//!
//! // in the first tick, input 4 is HIGH
//! let cont = t.tick(&state, &mut outputs);
//! assert_eq!(cont, Continuation::Continue);
//!
//! // then it goes low and the wait stage has completed
//! state.inputs.set(4, false);
//! let cont = t.tick(&state, &mut outputs);
//! assert_eq!(cont, Continuation::Continue);
//! // output 5 shouldn't be set yet
//! assert_eq!(outputs.0[5], false);
//!
//! // continuing onto the second stage, we should set output 5 HIGH
//! let cont = t.tick(&state, &mut outputs);
//! assert_eq!(outputs.0[5], true);
//!
//! // and we're all done
//! assert_eq!(cont, Continuation::Exit);
//!
//! ```

mod limits;
mod set_output;
mod wait_until;

pub use self::limits::AxisLimits;
pub use self::set_output::set_output;
pub use self::wait_until::wait_until;

/// Trigger for controlling axis motion.
pub trait AxisMotion {
    fn set_limit_switch(&mut self, axis_no: usize, is_on_limits: bool);
}

/// Trigger for updating digital outputs.
pub trait DigitalOutput {
    fn set_output(&mut self, output: usize, state: bool);
}
