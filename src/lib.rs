#![no_std]

#[cfg(test)]
#[macro_use]
extern crate std;

pub mod combinators;
pub mod state;
pub mod tasks;

pub use crate::combinators::TaskExt;
pub use crate::state::State;

/// A state machine which will be periodically polled with a snapshot of the
/// world and can interact with the world via `triggers`.
pub trait Task<A, S = State>
where
    A: ?Sized,
    S: ?Sized,
{
    /// Poll the task once.
    fn tick(&mut self, state: &S, triggers: &mut A) -> Continuation;
}

impl<A, S, F> Task<A, S> for F
where
    F: FnMut(&S, &mut A) -> Continuation,
    A: ?Sized,
    S: ?Sized,
{
    fn tick(&mut self, state: &S, triggers: &mut A) -> Continuation {
        (*self)(state, triggers)
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Continuation {
    /// Keep polling this task.
    Continue,
    /// The task has reached an end state (i.e. completed successfully or
    /// faulted).
    Exit,
}
