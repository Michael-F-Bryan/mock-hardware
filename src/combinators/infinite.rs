use crate::{Continuation, Task};

/// A task which will run forever.
#[derive(Debug, Clone, PartialEq)]
pub struct RunForever<T>(T);

impl<T> RunForever<T> {
    pub(crate) fn new(inner: T) -> RunForever<T> {
        RunForever(inner)
    }
}

impl<T, A, S> Task<A, S> for RunForever<T>
where
    T: ResettableTask<A, S>,
    A: ?Sized,
    S: ?Sized,
{
    fn tick(&mut self, state: &S, triggers: &mut A) -> Continuation {
        let cont = self.0.tick(state, triggers);

        if cont == Continuation::Exit {
            self.0.reset();
        }

        Continuation::Continue
    }
}

pub trait ResettableTask<A, S>: Task<A, S>
where
    A: ?Sized,
    S: ?Sized,
{
    fn reset(&mut self);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Debug, Default)]
    struct Dummy {
        resets: usize,
    }

    impl Task<(), bool> for Dummy {
        fn tick(&mut self, state: &bool, _triggers: &mut ()) -> Continuation {
            if *state {
                Continuation::Continue
            } else {
                Continuation::Exit
            }
        }
    }

    impl ResettableTask<(), bool> for Dummy {
        fn reset(&mut self) {
            self.resets += 1;
        }
    }

    #[test]
    fn run_and_automatically_reset() {
        let mut rf = RunForever(Dummy::default());
        let mut keep_going = true;

        let cont = rf.tick(&keep_going, &mut ());
        assert_eq!(cont, Continuation::Continue);
        assert_eq!(rf.0.resets, 0);

        // tell the inner task to stop
        keep_going = false;
        let cont = rf.tick(&keep_going, &mut ());
        assert_eq!(cont, Continuation::Continue);
        assert_eq!(rf.0.resets, 1);

        keep_going = true;
        let cont = rf.tick(&keep_going, &mut ());
        assert_eq!(cont, Continuation::Continue);
        assert_eq!(rf.0.resets, 1);
    }
}
