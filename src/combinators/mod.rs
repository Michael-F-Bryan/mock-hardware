//! Combinators for building up more complex state machines out of less complex
//! ones.

mod and_then;
mod infinite;

pub use self::and_then::AndThen;
pub use self::infinite::{ResettableTask, RunForever};

use crate::Task;

/// Useful extension methods for combining tasks.
pub trait TaskExt<A, S>: Task<A, S>
where
    A: ?Sized,
    S: ?Sized,
    Self: Sized,
{
    /// Execute a second task after this one has completed.
    fn and_then<Q: Task<A, S>>(self, other: Q) -> AndThen<Self, Q> {
        AndThen::new(self, other)
    }

    fn run_forever(self) -> RunForever<Self> {
        RunForever::new(self)
    }
}

impl<T, A, S> TaskExt<A, S> for T where T: Task<A, S> {}
