use crate::{Continuation, Task};

/// The combinator returned by `TaskExt::and_then()`.
pub struct AndThen<First, Next> {
    first: First,
    next: Next,
    state: AndThenState,
}

impl<First, Next> AndThen<First, Next> {
    pub(crate) fn new(first: First, next: Next) -> AndThen<First, Next> {
        AndThen {
            first,
            next,
            state: AndThenState::First,
        }
    }
}

impl<First, Next, S, A> Task<A, S> for AndThen<First, Next>
where
    First: Task<A, S>,
    Next: Task<A, S>,
{
    fn tick(&mut self, state: &S, triggers: &mut A) -> Continuation {
        match self.state {
            AndThenState::First => {
                let cont = self.first.tick(state, triggers);

                if cont == Continuation::Exit {
                    self.state = AndThenState::Next;
                }
                Continuation::Continue
            }
            AndThenState::Next => match self.next.tick(state, triggers) {
                Continuation::Continue => Continuation::Continue,
                Continuation::Exit => {
                    self.state = AndThenState::Done;
                    Continuation::Exit
                }
            },
            AndThenState::Done => Continuation::Exit,
        }
    }
}

enum AndThenState {
    First,
    Next,
    Done,
}
