# Mock Indexer Hardware

[![pipeline status](https://gitlab.com/Michael-F-Bryan/mock-hardware/badges/master/pipeline.svg)](https://gitlab.com/Michael-F-Bryan/mock-hardware/commits/master)

**([API Docs](https://michael-f-bryan.gitlab.io/mock-hardware))**

Code for simulating the machine an indexer is talking to.

## License

This project is in no way affiliated with *Wintech Engineering* and done 
completely in my own time. As such it is licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or
  http://opensource.org/licenses/MIT)

at your option.